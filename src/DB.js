class DB {
  constructor(collections) {
    if (
      collections !== null &&
      collections !== undefined &&
      collections !== []
    ) {
      //this._db = collections.map((s, i) => ({ ...s, id: i }));
      if (collections instanceof Object) {
        this._db = collections;
        console.log(collections);
        Object.values(this._db).forEach(el => {
          if (!(el instanceof Collection)) {
            throw new Error(
              "TinyDB: You supplied a value that is not an instanceof a collection"
            );
          }
        });
      } else {
        throw new Error("eh");
      }
    } else {
      this._db = {};
    }
  }
}

export default DB;