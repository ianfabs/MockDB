class Collection {
  constructor(set) {
    if (set !== null && set !== undefined && set !== []) {
      this._c = set.map((s, i) => ({ ...s, id: i }));
    } else {
      this._c = [];
    }
  }
  get items() {
    return this._c.map((s, i) => ({ ...s, id: i }));
  }
  findByKey(key, value) {
    return this._c.filter(el => el[key] === value);
  }
  findByKeyLike(key, value) {
    return this._c.filter(el => el[key].match(value));
  }
  findById(id) {
    return this._c[id];
  }
  findByIdAndUpdate(id, item) {
    this._c[id] = { ...this._c[id], ...item };
    return this._c[id];
  }
  findByIdAndRemove(id) {
    if (this._c.length > -1) {
      this._c.splice(id, 1);
      return true;
    } else return false;
  }
  findAndRemoveDuplicatesByKey(key) {
    const values = this._c.map(e => e[key]);
    this._c = this._c.filter((el, i) => values.indexOf(el[key]) === i);
    return this._c;
  }
  insert(item) {
    const _new = { ...item, id: this._c.length };
    this._c.push(_new);
    return _new;
  }
}

export default Collection;